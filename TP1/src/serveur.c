#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Portabilite.h"

/* Programme serveur */

int main(int argc, char *argv[])
{
	init();
	
	/* Je passe en paramètre le numéro de port qui sera donné à la socket créée plus loin.*/
	
	/* Je teste le passage de parametres. Le nombre et la nature des
	paramètres sont à adapter en fonction des besoins. Sans ces
	paramètres, l'exécution doit être arrétée, autrement, elle
	aboutira à des erreurs.*/
	if(argc != 2)
	{
		printf("utilisation : %s port_serveur\n", argv[0]);
		exit(1);
	}
	
	/* Etape 1 : créer une socket */   
	int ds = socket(PF_INET, SOCK_DGRAM, 0);
	
	/* /!\ : Il est indispensable de tester les valeurs de retour de
	toutes les fonctions et agir en fonction des valeurs
	possibles. Voici un exemple */
	if (ds == -1)
	{
		perror("Serveur : pb creation socket :");
		exit(1); // je choisis ici d'arrêter le programme car le reste
		// dépendent de la réussite de la création de la socket.
	}
	
	/* J'ajoute des traces pour comprendre l'exécution et savoir
	localiser des éventuelles erreurs */
	printf("Serveur : creation de la socket réussie \n");
	
	// Je peux tester l'exécution de cette étape avant de passer à la
	// suite. Faire de même pour la suite : n'attendez pas de tout faire
	// avant de tester.
	
	/* Etape 2 : Nommer la socket du seveur */
	
	struct sockaddr_in ad;
	ad.sin_family = AF_INET;
	ad.sin_addr.s_addr = INADDR_ANY;
	ad.sin_port = htons((short)atoi(argv[1]));
	
	int res1 = bind(ds, (struct sockaddr*)&ad, sizeof(ad));
	
	if(res1 == -1)
		gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors du nommage de la socket.");
	
	/* Etape 4 : recevoir un message du client (voir sujet pour plus de détails)*/
	
	struct sockaddr_in socketClient;
	socklen_t longueurSocketClient = sizeof(struct sockaddr);
	
	char message[100];
	
	fprintf(stdout, "Attente de la reception d'un message de la part du client.\n");
	
	ssize_t res2 = recvfrom(ds, (void*)message, sizeof(char)+100, 0, (struct sockaddr*)&socketClient, &longueurSocketClient);
	
	if(res2 == -1)
		gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de la reception d'un message de la part du client.");
	
	fprintf(stdout, "Message : %s, Recu de : %s:%d.\n", message, inet_ntoa(socketClient.sin_addr), (int)ntohs(socketClient.sin_port));
	
	/* Etape 5 : envoyer un message au serveur (voir sujet pour plus de détails)*/
	
	int nbOctetsRecus = (int)res2;
	
	res2 = sendto(ds, (void*)&nbOctetsRecus, sizeof(int), 0, (struct sockaddr*)&socketClient, longueurSocketClient);
	
	if(res2 == -1)
		gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de l'envoi d'un message au client.");
	
	fprintf(stdout, "Nous avons recu %d octet(s) de la part du client, ce nombre vient de lui etre envoye.\n", nbOctetsRecus);
	
	/* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
	
	close(ds);
	
	
	printf("Serveur : je termine\n");
	
	end();
	
	return 0;
}
