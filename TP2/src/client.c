#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include "Portabilite.h"

/* Programme client */

int sendTCP(int sock, void* msg, int sizeMsg)
{
	ssize_t res = send(sock, msg, sizeMsg, 0);
	
	if(res <= 0)
		return res;
	else if(res == sizeMsg)
		return 1;
	
	return sendTCP(sock, msg+res, sizeMsg-(int)res);
}

int main(int argc, char *argv[])
{
	init();
	
	/* je passe en paramètre l'adresse de la socket du serveur (IP et
	numéro de port) et un numéro de port à donner à la socket créée plus loin.*/
	
	/* Je teste le passage de parametres. Le nombre et la nature des
	paramètres sont à adapter en fonction des besoins. Sans ces
	paramètres, l'exécution doit être arrétée, autrement, elle
	aboutira à des erreurs.*/
	if(argc != 3)
	{
		printf("utilisation : %s ip_serveur port_serveur nombre_de_messages\n", argv[0]);
		exit(1);
	}
	
	//int compteur = atoi(argv[3]);
	
	/* Etape 1 : créer une socket */   
	int ds = socket(PF_INET, SOCK_STREAM, 0);
	
	/* /!\ : Il est indispensable de tester les valeurs de retour de
	toutes les fonctions et agir en fonction des valeurs
	possibles. Voici un exemple */
	if(ds == -1)
	{
		perror("Client : pb creation socket :");
		exit(1); // je choisis ici d'arrêter le programme car le reste
		// dépendent de la réussite de la création de la socket.
	}
	
	/* J'ajoute des traces pour comprendre l'exécution et savoir
	localiser des éventuelles erreurs */
	printf("Client : creation de la socket réussie \n");
	
	// Je peux tester l'exécution de cette étape avant de passer à la
	// suite. Faire de même pour la suite : n'attendez pas de tout faire
	// avant de tester.
	
	/* Etape 2 : Nommer la socket du client */
	
	/*struct sockaddr_in ad;
	ad.sin_family = AF_INET;
	ad.sin_addr.s_addr = INADDR_ANY;
	ad.sin_port = htons((short)atoi(argv[3]));
	
	int res1 = bind(ds, (struct sockaddr*)&ad, sizeof(ad));
	
	if(res1 == -1)
		gestionErreurs("ERREUR : Client : Une erreur s'est produite lors du nommage de la socket.");*/
	
	/* Etape 3 : Désigner la socket du serveur */
	
	struct sockaddr_in socketDistante;
	socketDistante.sin_family = AF_INET;
	socketDistante.sin_addr.s_addr = inet_addr(argv[1]);
	socketDistante.sin_port = htons((short)atoi(argv[2]));
	
	// Demande de connexion à un serveur.
	
	socklen_t tailleAdSocketDistante = sizeof(struct sockaddr_in);
	
	int res1 = connect(ds, (struct sockaddr*)&socketDistante, tailleAdSocketDistante);
	
	if(res1 == -1)
		gestionErreurs("ERREUR : Client : Une erreur s'est produite lors de la demande de connexion de la socket.");
	
	/* Etape 4 : envoyer un message au serveur  (voir sujet pour plus de détails)*/
	
	while(1)
	{
		int tailleMaximaleMessage = 16000;
		
		char msg[(tailleMaximaleMessage + 1)];
		
		msg[0] = '\0';
		
		fprintf(stdout, "Veuillez saisir votre message (%lu caracteres maximum) :\n", (long unsigned int)(sizeof(msg) - 1));
		
		char* res = fgets(msg, tailleMaximaleMessage, stdin);
		
		res = res ? res : res;
		
		int tailleReelleMessage = strlen(msg);
		
		ssize_t res2 = send(ds, (void*)&tailleReelleMessage, sizeof(tailleReelleMessage), 0);
		
		if(res2 == -1)
			gestionErreurs("ERREUR : Client : Une erreur s'est produite lors de l'envoi de la taille du message au serveur.\n");
		
		fprintf(stdout, "Taille du message envoye au serveur.\n");
		
		/*for(int i=0;i<compteur;++i)
		{
			res2 = send(ds, (void*)msg, strlen(msg)+1, 0);
			
			if(res2 == -1)
				gestionErreurs("ERREUR : Client : Une erreur s'est produite lors de l'envoi d'un message au serveur.\n");
			
			fprintf(stdout, "Message %d envoye au serveur.\n", i);
		}*/
		
		res2 = sendTCP(ds, (void*)msg, strlen(msg)+1);
		
		if(res2 == -1)
			gestionErreurs("ERREUR : Client : Une erreur s'est produite lors de l'envoi d'un message au serveur.\n");
		
		fprintf(stdout, "Message envoye au serveur.\n");
		
		/* Etape 5 : recevoir un message du serveur (voir sujet pour plus de détails)*/
		
		int msgRecu;
		
		res2 = recv(ds, (void*)&msgRecu, sizeof(int), 0);
		
		if(res2 == -1)
			gestionErreurs("ERREUR : Client : Une erreur s'est produite lors de la reception d'un message depuis le serveur.");
		
		fprintf(stdout, "Le nombre d'octets qu'a recu le serveur de notre part est de : %d octet(s).\n", msgRecu);
		fprintf(stdout, "L'adresse et le port de l'expediteur du message que nous venons de recevoir sont les suivants : %s:%d.\n", inet_ntoa(socketDistante.sin_addr), (int)ntohs(socketDistante.sin_port));
	}
	
	/* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
	
	close(ds);
	
	printf("Client : je termine\n");
	
	end();
	
	return 0;
}
