#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Portabilite.h"

/* Programme serveur */

int recvTCP(int sock, void* msg, int sizeMsg)
{
	ssize_t res = recv(sock, msg, sizeMsg, 0);
	
	if(res <= 0)
		return res;
	else if(res == sizeMsg)
		return 1;
	
	return recvTCP(sock, msg+res, sizeMsg-(int)res);
}

int main(int argc, char *argv[])
{
	init();
	
	/* Je passe en paramètre le numéro de port qui sera donné à la socket créée plus loin.*/
	
	/* Je teste le passage de parametres. Le nombre et la nature des
	paramètres sont à adapter en fonction des besoins. Sans ces
	paramètres, l'exécution doit être arrétée, autrement, elle
	aboutira à des erreurs.*/
	if(argc != 2)
	{
		printf("utilisation : %s port_serveur\n", argv[0]);
		exit(1);
	}
	
	/* Etape 1 : créer une socket */   
	int ds = socket(PF_INET, SOCK_STREAM, 0);
	
	/* /!\ : Il est indispensable de tester les valeurs de retour de
	toutes les fonctions et agir en fonction des valeurs
	possibles. Voici un exemple */
	if (ds == -1)
	{
		perror("Serveur : pb creation socket :");
		exit(1); // je choisis ici d'arrêter le programme car le reste
		// dépendent de la réussite de la création de la socket.
	}
	
	/* J'ajoute des traces pour comprendre l'exécution et savoir
	localiser des éventuelles erreurs */
	printf("Serveur : creation de la socket réussie \n");
	
	// Je peux tester l'exécution de cette étape avant de passer à la
	// suite. Faire de même pour la suite : n'attendez pas de tout faire
	// avant de tester.
	
	/* Etape 2 : Nommer la socket du seveur */
	
	struct sockaddr_in ad;
	ad.sin_family = AF_INET;
	ad.sin_addr.s_addr = INADDR_ANY;
	ad.sin_port = htons((short)atoi(argv[1]));
	
	int res1 = bind(ds, (struct sockaddr*)&ad, sizeof(ad));
	
	if(res1 == -1)
		gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors du nommage de la socket.");
	
	// Mise de la socket en mode écoute.
	
	printf("Mise en ecoute de la socket du serveur.\n");
	
	int nombreConnexionMax = 10;
	
	res1 = listen(ds, nombreConnexionMax);
	
	if(res1 == -1)
		gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de la mise en ecoute de la socket.");
	
	printf("Mise en ecoute de la socket du serveur reussie.\n");
	
	while(1)
	{
		// Accepter une connexion d'un client.
		
		printf("Acceptation de la demande de connexion d'un client ; attente d'une demande de connexion.\n");
		
		struct sockaddr_in adClient;
		socklen_t tailleAdClient = sizeof(struct sockaddr_in);
		
		res1 = accept(ds, (struct sockaddr*)&adClient, &tailleAdClient);
		
		if(res1 == -1)
			gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de l'acceptation d'une demande de connexion a la socket.");
		
		int socketClient = res1;
		
		int pere = getpid();
		
		int resFork;
		
		if((resFork = fork()) == -1)
		{
			fprintf(stderr, "ERREUR : Serveur : Une erreur s'est produite lors de la création du fork. Erreur du systeme : %s.\n", strerror(errno));
			exit(1);
		}
		else if(pere != getpid())
		{
			fprintf(stdout, "Je suis le serveur du processus fils.\nJe commence le traitement des messages du client connecte %s:%d.\n", inet_ntoa(adClient.sin_addr), (int)ntohs(adClient.sin_port));
			
			while(1)
			{
				printf("Demande de connexion d'un client acceptee.\n");
				
				/* Etape 4 : recevoir un message du client (voir sujet pour plus de détails)*/
				
				fprintf(stdout, "Attente de la reception d'un message de la part du client.\n");
				
				int tailleMaximaleARecevoir;
				
				ssize_t res2 = recv(socketClient, (void*)&tailleMaximaleARecevoir, sizeof(int), 0);
				
				if(res2 == -1)
					gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de la reception de la taille du message de la part du client.");
				
				fprintf(stdout, "Taille du message a recevoir recue.\n");
				
				char message[tailleMaximaleARecevoir];
				
				/*while(1)
				{
					res2 = recv(socketClient, (void*)message, sizeof(message), 0);
					
					if(res2 == -1)
						gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de la reception d'un message de la part du client.");
					
					fprintf(stdout, "Message : %s, Recu de : %s:%d.\n", message, inet_ntoa(adClient.sin_addr), (int)ntohs(adClient.sin_port));
					
					if(res2 == 0)
						break;
				}*/
				
				res2 = recvTCP(socketClient, (void*)message, sizeof(message));
				
				if(res2 == -1)
					gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de la reception d'un message de la part du client.");
				
				fprintf(stdout, "Message : %s, Recu de : %s:%d.\n", message, inet_ntoa(adClient.sin_addr), (int)ntohs(adClient.sin_port));
				
				/* Etape 5 : envoyer un message au serveur (voir sujet pour plus de détails)*/
				
				printf("Envoi du nombre d'octets recus de la part du client au client.\n");
				
				int nbOctetsRecus = (int)res2;
				
				res2 = send(socketClient, (void*)&nbOctetsRecus, sizeof(int), 0);
				
				if(res2 == -1)
					gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de l'envoi d'un message au client.");
				
				fprintf(stdout, "Nous avons recu %d octet(s) de la part du client, ce nombre vient de lui etre envoye.\n", nbOctetsRecus);
			}
		}
	}
	
	/* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
	
	//close(socketClient);
	close(ds);
	
	
	printf("Serveur : je termine\n");
	
	end();
	
	return 0;
}
