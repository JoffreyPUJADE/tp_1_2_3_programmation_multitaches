#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include "Portabilite.h"

int main(int argc, char *argv[])
{
	init();
	
	if(argc != 4)
	{
		fprintf(stderr, "utilisation : %s ip_serveur port_serveur port_client\n", argv[0]);
		exit(1);
	}
	
	// Création de la socket.
	
	int ds = socket(AF_INET, SOCK_DGRAM, 0);
	
	if(ds == -1)
	{
		perror("Client : pb creation socket :");
		exit(1);
	}
	
	printf("%s : creation de la socket réussie.\n", argv[0]);
	
	// Nommage de la socket.
	
	struct sockaddr_in ad;
	ad.sin_family = AF_INET;
	ad.sin_addr.s_addr = INADDR_ANY;
	ad.sin_port = htons((short)atoi(argv[3]));
	
	int res1 = bind(ds, (struct sockaddr*)&ad, sizeof(ad));
	
	if(res1 == -1)
		gestionErreurs("ERREUR : Client : Une erreur s'est produite lors du nommage de la socket.");
	
	// Désignation de la socket du serveur.
	
	struct sockaddr_in socketDistante;
	socketDistante.sin_family = AF_INET;
	socketDistante.sin_addr.s_addr = inet_addr(argv[1]);
	socketDistante.sin_port = htons((short)atoi(argv[2]));
	
	// Envoi d'un message au serveur.
	
	/*char msg[101];
	
	msg[0] = '\0';
	
	fprintf(stdout, "Veuillez saisir votre message (100 caracteres maximum) :\n");
	fgets(msg, 100, stdin);*/
	
	//int tabAEnvoyer[20] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
	
	//ssize_t res2 = sendto(ds, (void*)&msg, strlen(msg)+1, 0, (struct sockaddr*)&socketDistante, sizeof(struct sockaddr_in));
	//ssize_t res2 = sendto(ds, (void*)&tabAEnvoyer, sizeof(tabAEnvoyer), 0, (struct sockaddr*)&socketDistante, sizeof(struct sockaddr_in));
	
	/*if(res2 == -1)
		gestionErreurs("ERREUR : Client : Une erreur s'est produite lors de l'envoi d'un message au serveur.\n");
	
	fprintf(stdout, "Message envoye au serveur.\n");*/
	
	/*ssize_t res2;
	
	for(int i=0;i<20;++i)
	{
		res2 = sendto(ds, (void*)&socketDistante, sizeof(struct sockaddr_in), 0, (struct sockaddr*)&socketDistante, sizeof(struct sockaddr_in));
	
		if(res2 == -1)
			gestionErreurs("ERREUR : Client : Une erreur s'est produite lors de l'envoi d'un message au serveur.\n");
		
		fprintf(stdout, "Message %d envoye au serveur.\n", i);
	}*/
	
	/*int tabAEnvoyer[20] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
	
	ssize_t res2;
	
	for(int i=0;i<20;++i)
	{
		res2 = sendto(ds, (void*)&tabAEnvoyer[i], sizeof(int), 0, (struct sockaddr*)&socketDistante, sizeof(struct sockaddr_in));
	
		if(res2 == -1)
			gestionErreurs("ERREUR : Client : Une erreur s'est produite lors de l'envoi d'un message au serveur.\n");
		
		fprintf(stdout, "Message %d envoye au serveur.\n", i);
	}*/
	
	/*long unsigned int tailleTabAEnvoyer = 8750;
	double tabAEnvoyer[tailleTabAEnvoyer];
	
	for(long unsigned int i=0;i<tailleTabAEnvoyer;++i)
	{
		tabAEnvoyer[i] = (double)i;
	}
	
	printf("Tableau initialise.\n");
	
	ssize_t res2 = sendto(ds, (void*)&tabAEnvoyer, sizeof(double)*tailleTabAEnvoyer, 0, (struct sockaddr*)&socketDistante, sizeof(struct sockaddr_in));
	
	if(res2 == -1)
		gestionErreurs("ERREUR : Client : Une erreur s'est produite lors de l'envoi d'un message au serveur.\n");
	
	fprintf(stdout, "Message envoye au serveur.\n");*/
	
	char msg[101];
	
	msg[0] = '\0';
	
	fprintf(stdout, "Veuillez saisir votre message (100 caracteres maximum) :\n");
	char* res = fgets(msg, 100, stdin);
	res = res ? res : res;
	
	ssize_t res2 = sendto(ds, (void*)&msg, strlen(msg)+1, 0, (struct sockaddr*)&socketDistante, sizeof(struct sockaddr_in));
	
	if(res2 == -1)
		gestionErreurs("ERREUR : Client : Une erreur s'est produite lors de l'envoi d'un message au serveur.\n");
	
	fprintf(stdout, "Message envoye au serveur.\n");
	
	// Réception d'un message de la part du serveur.
	
	int msgRecu;
	
	socklen_t tailleSocketDistante = sizeof(struct sockaddr);
	
	res2 = recvfrom(ds, (void*)&msgRecu, sizeof(int), 0, (struct sockaddr*)&socketDistante, &tailleSocketDistante);
	
	if(res2 == -1)
		gestionErreurs("ERREUR : Client : Une erreur s'est produite lors de la reception d'un message depuis le serveur.");
	
	fprintf(stdout, "Le nombre d'octets qu'a recu le serveur de notre part est de : %d octet(s).\n", msgRecu);
	fprintf(stdout, "L'adresse et le port de l'expediteur du message que nous venons de recevoir sont les suivants : %s:%d.\n", inet_ntoa(socketDistante.sin_addr), (int)ntohs(socketDistante.sin_port));
	
	// Fermeture de la socket.
	
	close(ds);
	
	printf("Client : je termine\n");
	
	end();
	
	return 0;
}
