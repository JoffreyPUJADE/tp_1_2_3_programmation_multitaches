#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Portabilite.h"

int main(int argc, char *argv[])
{
	init();
	
	// Création de la socket.
	
	int ds = socket(AF_INET, SOCK_DGRAM, 0);
	
	if(ds == -1)
	{
		perror("Serveur : pb creation socket :");
		exit(1);
	}
	
	printf("Serveur : creation de la socket réussie \n");
	
	// Nommage de la socket.
	
	socklen_t tailleSockAddrIn = sizeof(struct sockaddr_in);
	struct sockaddr_in ad;
	ad.sin_family = AF_INET;
	ad.sin_addr.s_addr = INADDR_ANY;
	
	if(argc == 1)
		ad.sin_port = htons((short)0);
	else
		ad.sin_port = htons((short)atoi(argv[1]));
	
	int res1 = bind(ds, (struct sockaddr*)&ad, sizeof(ad));
	
	if(res1 == -1)
		gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors du nommage de la socket.");
	
	res1 = getsockname(ds, (struct sockaddr*)&ad, &tailleSockAddrIn);
	
	fprintf(stdout, "Serveur nomme avec l'adresse et le port suivants : %s:%d.\n", inet_ntoa(ad.sin_addr), (int)ntohs(ad.sin_port));
	
	// Réception d'un message de la part du client.
	struct sockaddr_in socketClient;
	socklen_t longueurSocketClient = sizeof(struct sockaddr);
	
	fprintf(stdout, "Attente de la reception d'un message de la part du client.\n");
	
	//char message[100];
	//int tabARecevoir[20];
	
	//ssize_t res2 = recvfrom(ds, (void*)message, sizeof(char)+100, 0, (struct sockaddr*)&socketClient, &longueurSocketClient);
	//ssize_t res2 = recvfrom(ds, (void*)tabARecevoir, sizeof(tabARecevoir), 0, (struct sockaddr*)&socketClient, &longueurSocketClient);
	
	/*if(res2 == -1)
		gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de la reception d'un message de la part du client.");*/
	
	/*for(int i=0;i<20;++i)
		printf("%d, ", tabARecevoir[i]);
	printf("\n");*/
	
	//fprintf(stdout, "Message : %d, Recu de : %s:%d.\n", tabARecevoir[0], inet_ntoa(socketClient.sin_addr), (int)ntohs(socketClient.sin_port));
	
	/*
	struct sockaddr_in tabARecevoir[20];
	
	int cmptOctets = 0;
	ssize_t res2;
	
	for(int i=0;i<20;++i)
	{
		res2 = recvfrom(ds, (void*)&tabARecevoir[i], sizeof(struct sockaddr_in), 0, (struct sockaddr*)&socketClient, &longueurSocketClient);
		
		if(res2 == -1)
			gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de la reception d'un message de la part du client.");
		
		cmptOctets += (int)res2;
		
		fprintf(stdout, "Message : %s:%d, Recu de : %s:%d.\n", inet_ntoa(tabARecevoir[i].sin_addr), (int)ntohs(tabARecevoir[i].sin_port), inet_ntoa(socketClient.sin_addr), (int)ntohs(socketClient.sin_port));
	}*/
	
	/*char msg[1];
	int res = scanf("%s", msg);
	res = res ? res : res;
	
	int tabARecevoir[20];
	
	int cmptOctets = 0;
	ssize_t res2;
	
	for(int i=0;i<20;++i)
	{
		res2 = recvfrom(ds, (void*)&tabARecevoir[i], sizeof(int), 0, (struct sockaddr*)&socketClient, &longueurSocketClient);
		
		if(res2 == -1)
			gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de la reception d'un message de la part du client.");
		
		cmptOctets += (int)res2;
		
		fprintf(stdout, "Message : %d, Recu de : %s:%d.\n", tabARecevoir[i], inet_ntoa(socketClient.sin_addr), (int)ntohs(socketClient.sin_port));
	}*/
	
	/*long unsigned int tailleTabARecevoir = 8750;
	double tabARecevoir[tailleTabARecevoir];
	
	ssize_t res2 = recvfrom(ds, (void*)&tabARecevoir, sizeof(double)*tailleTabARecevoir, 0, (struct sockaddr*)&socketClient, &longueurSocketClient);
		
	if(res2 == -1)
		gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de la reception d'un message de la part du client.");
	
	fprintf(stdout, "Message recu de : %s:%d.\n", inet_ntoa(socketClient.sin_addr), (int)ntohs(socketClient.sin_port));*/
	
	char message[100];
	
	ssize_t res2 = recvfrom(ds, (void*)message, sizeof(char)*100, 0, (struct sockaddr*)&socketClient, &longueurSocketClient);
	
	if(res2 == -1)
		gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de la reception d'un message de la part du client.");
	
	fprintf(stdout, "Message : %s, Recu de : %s:%d.\n", message, inet_ntoa(socketClient.sin_addr), (int)ntohs(socketClient.sin_port));
	
	// Envoi d'un message au serveur.
	
	int nbOctetsRecus = (int)res2;
	
	res2 = sendto(ds, (void*)&nbOctetsRecus, sizeof(int), 0, (struct sockaddr*)&socketClient, longueurSocketClient);
	
	if(res2 == -1)
		gestionErreurs("ERREUR : Serveur : Une erreur s'est produite lors de l'envoi d'un message au client.");
	
	fprintf(stdout, "Nous avons recu %d octet(s) de la part du client, ce nombre vient de lui etre envoye.\n", nbOctetsRecus);
	
	// Fermeture de la socket.
	
	close(ds);
	
	printf("Serveur : je termine\n");
	
	end();
	
	return 0;
}
