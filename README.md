# TP 1, 2 et 3 - Programmation Multitaches

# Introduction

Ce dépôt contient les TP 1, 2 et 3 de l'UE de Programmation Multitaches, réalisés pour les révisions
du premier TP noté. Ces TP sont portables entre Windows et Linux (jusqu'au TP 2 inclus pour le moment).

# Sommaire

* [Portabilité entre Linux et Windows](#portabilite-entre-linux-et-windows)
* [TP 1](#tp-1)

# Portabilité entre Linux et Windows

Afin de pouvoir réussir à faire un code portable entre Linux et Windows, merci de suivre les instructions
suivante.

Sur chaque fichier issu des fichiers de base des TP fournis par la prof, il faut retirer ces inclusions :

```c
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include<arpa/inet.h>
```

Et remplacer ça par :

```c
#include "Portabilite.h"
```

Également, il faut rajouter, au tout début de la fonction `main`, avant TOUTE chose :

```c
init();
```

Et à la TOUTE fin de la fonction `main`, juste avant la ligne `return 0;` :

```c
end();
```

Ensuite, tous les fichiers sources devront être placés dans un répertoire "`src`", et le `Makefile` ainsi que le fichier "`make.bat`" devront être, eux, placés à la racine du répertoire.

Ensuite, il suffira juste de taper la commande `make` dans un terminal de commande pour compiler les fichiers sources et pouvoir les utiliser.

Le fichier `Portabilite.h` permet de réaliser la portabilité entre les OS Linux et Windows. Le `Makefile`, quant à lui, permet de compiler sous Linux et sous Windows (à la condition de posséder `MinGW`). Enfin, le fichier `make.bat` contient une commande permettant de faciliter l'usage du `Makefile` sous Windows.

# TP 1